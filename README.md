# ics_download

[download ics](https://gitlab.com/jgroom33/ics_download/-/raw/master/foo.ics?inline=false&filename="foo.ics")

[download txt](https://gitlab.com/jgroom33/ics_download/-/raw/master/foo.txt?inline=false)

[download json](https://gitlab.com/jgroom33/ics_download/-/raw/master/foo.json?inline=false)

[download js](https://gitlab.com/jgroom33/ics_download/-/raw/master/foo.js?inline=false)
